import asyncio

from binance.client import AsyncClient

from .simple_worker import SimpleWorker
from .util.contstant import DEFAULT_TRADING_PAIRS

API_KEY = 'KEY'
API_SECRET = 'SECRET'


async def runner(trading_pairs=DEFAULT_TRADING_PAIRS):
    client = await AsyncClient.create(API_KEY, API_SECRET)
    worker = SimpleWorker(client=client, trading_pairs=trading_pairs)
    await worker.run()


if __name__ == '__main__':
    loop = asyncio.run(runner())

