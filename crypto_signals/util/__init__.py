from datetime import datetime
from typing import NamedTuple, List, Any, Optional


class KlineData(NamedTuple):
    trading_pair: str
    interval: str
    open: float
    high: float
    low: float
    close: float
    volume: float
    open_time: Optional[datetime] = None
    close_time: Optional[datetime] = None

    def __str__(self):
        return f'<{self.trading_pair} OPEN - {self.open} CLOSE - {self.close}>\n'


def get_klines_data(
        raw_data: List[List[Any]],
        trading_pair,
        interval
) -> List[KlineData]:
    return [
        KlineData(
            trading_pair=trading_pair,
            interval=interval,
            open=float(item[1]),
            high=float(item[2]),
            low=float(item[3]),
            close=float(item[4]),
            volume=float(item[5])
        ) for item in raw_data
    ]


def get_kline_type(kline: KlineData):
    pass

