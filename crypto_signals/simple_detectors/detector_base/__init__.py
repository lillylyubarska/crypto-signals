from abc import ABCMeta, abstractmethod
from typing import NamedTuple, Optional
from binance.client import AsyncClient


class DetectorResponse (NamedTuple):
    detector_type: str
    price_delta: float
    price_percent_delta: float
    prediction: str

    def __str__(self):
        return ''


class Detector(metaclass=ABCMeta):
    def __init__(self, trading_pair: str, interval, client: AsyncClient):
        self.trading_pair = trading_pair
        self.interval = interval
        self.client = client

    @abstractmethod
    async def detect(self) -> Optional[DetectorResponse]:
        ...

