from typing import Optional

from ..detector_base import Detector, DetectorResponse


class AbsorptionDetector(Detector):

    async def detect(self) -> Optional[DetectorResponse]:
        return None

