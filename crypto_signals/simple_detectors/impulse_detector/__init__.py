from typing import Optional

from ..detector_base import Detector, DetectorResponse
from ..impulse_detector.util import has_impulse
from ...util import get_klines_data


class ImpulseDetector(Detector):

    async def detect(self) -> Optional[DetectorResponse]:
        raw_data = await self.client.get_historical_klines(
            self.trading_pair, self.interval, "1 day ago UTC"
        )
        klines = get_klines_data(
           raw_data=raw_data,
           trading_pair=self.trading_pair,
           interval=self.interval
        )
        last_kline = klines[-1]
        if has_impulse(last_kline):
            print(f'IMPULSE')
        return None

