from ...util import KlineData

IMPULSE_PERCENT = 0.02


def has_impulse(kline: KlineData) -> bool:
    return (kline.close - kline.open) / kline.open > IMPULSE_PERCENT
