from abc import ABCMeta, abstractmethod


class Messenger(metaclass=ABCMeta):
    @abstractmethod
    async def send(self):
        ...
