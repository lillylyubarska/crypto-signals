import asyncio
from typing import List
from itertools import product

from ..simple_detectors.absorption_detector import AbsorptionDetector
from ..simple_detectors.detector_base import Detector
from ..simple_detectors.impulse_detector import ImpulseDetector
from ..util.contstant import DEFAULT_KLINE_INTERVALS

detector_types = [
    ImpulseDetector,
    AbsorptionDetector
]


class SimpleWorker:
    def __init__(self, trading_pairs, client):
        self.detectors: List[Detector] = []
        for interval, trading_pair, detector in product(
                DEFAULT_KLINE_INTERVALS, trading_pairs, detector_types
        ):
            self.detectors.append(
                detector(
                    client=client,
                    interval=interval,
                    trading_pair=trading_pair
                )
            )

    async def detect(self):
        for detector in self.detectors:
            print('---------------')
            if response := await detector.detect():
                print('GOT RESPONSE')

    async def run(self):
        while True:
            await self.detect()
            await asyncio.sleep(1)



